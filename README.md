# Joomla Ajax Chat

Modulo Joomla che permette agli utenti registrati di chattare tra loro in tempo reale.

## Funzionalit� sviluppate

- Elenco utenti registrati con relativo stato aggiornato in tempo reale
- Avvio chat con singolo utente
- Invio e ricezione messaggi in tempo reale
- Storico messaggi
- Identificazione parolacce
- Possibilit� di selezionare, lato admin, come devono essere salvati i messaggi(su db o su txt). Per la versione txt � stata realizzata tutta la struttura ma le funzioni sono vuote 
- Ottimizzazione mobile


### Installazione

I file presenti su questo repository contengono una copia dell'installazione Joomla che sta girando sul dominio http://joomla.mm-web.it, con il modulo chat gi� installato e funzionante.

Sono disponibili 11 utenti:

- un utente di tipo Super Users(Username: admin - Password: admin)
- 10 utenti di tipo Registered(Username: utente1, utente2, ecc... - Password: utente1, utente2, ecc...)

Il database si trova nella cartella "_src" dove � presente anche il pacchetto di installazione del solo modulo chat.

Per installare il modulo chat su un'altra installazione di Joomla occorre:

- Collegarsi al pannello di amministrazione come Super User
- Navigare in "Estensioni > Gestione > Installa"
- Caricare il pacchetto "mod_ajax_chat.zip" che si trova sul repository nella cartella "_src"
- Navigare in "Estensioni > Moduli > Ajax Chat"
- Impostare la posizione del modulo e lo stato "Pubblicato"
- Nel tab "Assegnazione menu" selezionare le pagine in cui il modulo verr� visualizzato
- Salvare




