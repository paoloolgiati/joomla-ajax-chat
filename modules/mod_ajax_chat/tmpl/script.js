(function($){
		
	function ajaxChat(){
		
		var t = this;
		t.docTitle = document.title;
		t.getMessagesTimeout = null;
		t.userIdWith = null;
		t.lastMessageId = 0;
				
		//Esegue le chiamate ajax dinamicamente verso il modulo chat
		t.request = function(fn, callback, data){
			
            var param = {
                    'option' : 'com_ajax',
                    'module' : 'ajax_chat',	
					'fn'     : fn,
                    'data'   : (typeof(data) === 'undefined' ? '' : data),
                    'format' : 'json'
                };
			
			$.ajax({
				type   		: 'POST',
				data   		: param,
				dataType	: 'json'
			})
			.always(function(jqXHR, textStatus) {
    			if(typeof(callback) === 'function'){
					callback(jqXHR.data);				
				}				
  			});
			
		}
		
		//Lista utenti che si aggiorna
		t.getUsers = function(init){
			
			//Ajax + callback
			t.request('getUsers', function(data){				
				
				//Verifica il tipo di ritorno per prevenire errori
				if(jQuery.type(data) === 'array'){
					
					//Salva gli utenti in un oggetto indicizzato in base all'id utente
					var users = {};				
					var online = 0;
					var newMessages = 0;
					$.each(data, function(key, value){

						//value.logged contiene il numero di sessioni attive
						if(value.logged !== '0'){
							online++;
							value.logged = 1;
						}else{
							value.logged = 0;
						}

						//Conta i nuovi messaggi se la chat con l'utente non è già aperta
						if(t.userIdWith !== value.id){
							newMessages += parseInt(value.new_messages);  
						}					

						//Aggiunge l'oggetto
						users[value.id] = value;

					});

					//Aggiorna il numero degli utenti online
					$('#ajax_chat_users_online').text(online);

					//Verifica se ci sono utenti validi
					var keys = Object.keys(users);
					var len = keys.length;				
					if(keys.length){

						//Rimuove il messaggio di errore
						$('#ajax_chat_no_uesers').remove();

						//Rimuove eventuali utenti non più validi
						$('#ajax_chat_users .ac_user_row').each(function(){
							var $e = $(this);
							var str = $e.attr('id');
							var n = str.lastIndexOf('_');
							var id = parseInt(str.substring(n + 1));						
							if(typeof(users[id]) === 'undefined'){
								$('#ajax_chat_user_' + id).remove();
							}
						});

						//Aggiunge eventuali nuovi utenti validi
						$.each(users, function(key, value){

							//Se l'utente non è presente lo aggiunge
							if($('#ajax_chat_user_' + key).length === 0){							
								var el = '<li id="ajax_chat_user_' + value.id + '" class="ac_user_row">' + 
											'<a href="#" class="ac_open_chat" data-id="' + value.id + '" data-name="' + value.name + '" data-logged="' + value.logged + '">' + 
												'<span class="ac_logged ac_logged_' + value.logged + ' ac_logged_user_' + value.id + '"></span>' + 
												value.name + 
												'<span class="ac_new_messages_user" style="display: ' + (value.new_messages === '0' ? 'none' : 'block') + '">' + value.new_messages + '</span>' + 
											'</a>' + 
										'</li>';					
								$('#ajax_chat_users ul').append(el);

							//Altrimenti aggiorna solo lo stato e il numero di nuovi messaggi
							}else{							
								$('#ajax_chat .ac_logged_user_' + value.id).removeClass('ac_logged_0 ac_logged_1').addClass('ac_logged_' + value.logged);
								$('#ajax_chat_user_' + value.id + ' .ac_new_messages_user').css('display', (value.new_messages === '0' ? 'none' : 'block')).text(value.new_messages);							
							}

						});

						//Al caricamento della pagina apre l'eventuale chat aperta in precedenza se la pagina è stata ricaricata o cambiata
						if(init === true){
							var openChat = sessionStorage.getItem('openChat');
							if(openChat !== null) {
								$('#ajax_chat_btn_open, #ajax_chat_user_' + openChat + ' .ac_open_chat').click();
							}
						}

						//Aggiorna il numero totale dei nuovi messaggi
						if(newMessages > 0){
							$('#ajax_chat_new_messages').text(newMessages).show();
							document.title = '(' + newMessages + ') ' + t.docTitle;
						}else{
							$('#ajax_chat_new_messages').hide().text('');
							document.title = t.docTitle;
						}

					//Nessun utente valido
					}else{
						//Inserisce il messaggio
						$('#ajax_chat_users ul').html('<li id="ajax_chat_no_uesers">Nessun utente disponibile</li>');
						//Nasconde il conteggio dei nuovi messaggi
						$('#ajax_chat_new_messages').hide().text('');
						document.title = t.docTitle;
					}
				
				//Errore ritorno
				}else{
					
					//Sessione scaduta
					if(data === 'auth_error'){
						location.reload(true);
						return false;
					}					
					
				}
				
				//Aggiorna la lista
				setTimeout(t.getUsers, 3000);
				
			});
			
		}
		
		//Lista messaggi(inviati e ricevuti a/da t.userIdWith) che si aggiorna in base all'id dell'ultimo messaggio visualizzato
		t.getMessages = function(){
			
			//Ajax + callback
			t.request('getMessages', function(data){
				
				//Verifica il tipo di ritorno per prevenire errori
				if(jQuery.type(data) === 'array'){
					
					//Cicla ogni messaggio ricevuto dalla chiamata e lo mostra a video
					$.each(data, function(key, value){
						
						//Verifica che il messaggio non sia già stato inserito
						if($('#ajax_chat_message_' + value.message_id).length === 0){
						
							//Verifica se il messaggio è di tipo ricevuto
							var recived = 0;					
							if(value.user_id_from === t.userIdWith){
								recived = 1;
							}

							//Mostra il nuovo messaggio	
							moment.locale('it');
							var date = moment(value.date_time).format('ddd D MMM YY, H:mm');
							$('#ajax_chat_text').append(
								'<div class="ac_message_date ac_message_date_recived_' + recived + '">' + date + '</div>' +
								'<div id="ajax_chat_message_' + value.message_id + '" class="ac_message ac_message_recived_' + recived + '">' + 
									value.message +
								'</div>'						
							);

							//Se il messaggio è di tipo ricevuto e nuovo, viene memorizzato nel db che è stato letto
							if(recived && value.new === '1'){
								t.request('setMessageDisplayed', null, value.message_id);
							}

							t.lastMessageId = value.message_id;	
						
						}

					});

					//Se ci sono nuovi messaggi scrolla fino all'ultimo
					if(data.length > 0){
						t.scrollToLastMessage();
					}
					
				//Errore ritorno
				}else{
					
					//Sessione scaduta
					if(data === 'auth_error'){
						location.reload(true);
						return false;
					}					
					
				}
				
				//Aggiorna la lista e memorizza il timeout in una variabile che servirà per stopparlo quando si esce dal pannello messaggi
				t.getMessagesTimeout = setTimeout(function() { t.getMessages() }, 1000);
				
			}, {'id': t.userIdWith, 'lastMesssageid': t.lastMessageId});	
			
		}
		
		//Invia un messaggio
		t.sendMessage = function(message){
			
			//Blocca invio messaggio vuoto
			if($.trim(message) === ''){
				return false;
			}
			
			//Blocca l'invio di nuovi messaggi fino al completamento dell'invio precedente
			$('#ajax_chat_form').addClass('loading');
			
			t.request('sendMessage', function(data){
				
				if(data === 'ok'){					
					//Resetta il campo
					$('#ajax_chat_input').val('');					
				}else{
					//Mostra l'errore
					$('#ajax_chat_text').append('<div class="ac_error_message">Errore invio messaggio</div>');
					t.scrollToLastMessage();
				}
				
				//Sblocca l'invio di nuovi messaggi
				$('#ajax_chat_form').removeClass('loading');
				
			}, {'id': t.userIdWith, 'message': $.trim(message)});
			
		}
		
		//Scrolla all'ultimo messaggio
		t.scrollToLastMessage = function(){
			$('#ajax_chat_text').animate({ scrollTop: $('#ajax_chat_text').prop('scrollHeight')}, 500);
		}
		
		//Resetta il pannello dei messaggi
		t.resetMessages = function(){
			$('#ajax_chat_messages').hide();
			$('#ajax_chat_name, #ajax_chat_text').html('');
			$('#ajax_chat_input').val('');
			clearTimeout(t.getMessagesTimeout);
			t.userIdWith = 0;
			t.lastMessageId = 0;
			sessionStorage.removeItem('openChat');
		}
		
		//Gestione eventi utente
		t.events = function(){
			
			//Click bottone apertura
			$('#ajax_chat_btn_open').click(function(){
				$('#ajax_chat_window').show();
			});
			
			//Click bottone chiusura
			$('#ajax_chat_btn_close').click(function(){
				$('#ajax_chat_window').hide();
				$('#ajax_chat_messages').hide();				
				$('#ajax_chat_users').show();
				t.resetMessages();
			});
			
			//Click bottone indietro
			$('#ajax_chat_btn_back').click(function(){
				$('#ajax_chat_messages').hide();				
				$('#ajax_chat_users').show();
				t.resetMessages();				
			});
			
			//Click nome utente
			$(document).on('click', '#ajax_chat .ac_open_chat', function(event){				
				event.preventDefault();
				var $e = $(this);
				t.resetMessages();				
				t.userIdWith = $e.data('id').toString();				
				$('#ajax_chat_users').hide();				
				$('#ajax_chat_messages').show();								
				$('#ajax_chat_name').html(
					$e.find('.ac_logged').clone().wrap("<div />").parent().html() +
					$e.data('name') 
				);
				setTimeout(function() { $('#ajax_chat_input').focus() }, 500);
				t.getMessages();
				sessionStorage.setItem('openChat', t.userIdWith);
				return false;
			});	
			
			//Invio messaggio
			$('#ajax_chat_form').submit(function(event){
				if($(this).hasClass('loading')){
					return false;
				}
				var message = $('#ajax_chat_input').val();
				t.sendMessage(message);
			  	event.preventDefault();
			});			
			
		}
		
		//Inizializzazione
		t.init = function(){			
			t.getUsers(true);
			t.events();				
		}
		
	}
	
	//Creazione oggetto
	$(document).ready(function(){
		var ac = new ajaxChat();
		ac.init();
	});
	
})(jQuery)