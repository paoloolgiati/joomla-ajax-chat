<?php defined('_JEXEC') or die; ?>

<?php

//Global document object
$doc = JFactory::getDocument();

//CSS 
$doc->addStyleSheet(JUri::base() . '/modules/mod_ajax_chat/tmpl/style.css');
$doc->addStyleSheet(JUri::base() . '/modules/mod_ajax_chat/tmpl/icons/css/all.min.css');

//JS
$doc->addScript(JUri::base() . '/modules/mod_ajax_chat/tmpl/script.js');
$doc->addScript(JUri::base() . '/modules/mod_ajax_chat/tmpl/moment-with-locales.min.js');
?>

<div id="ajax_chat">

	<div id="ajax_chat_window">	
		<div id="ajax_chat_top">
			<button type="button" id="ajax_chat_btn_close"><i class="fas fa-times"></i></button>
		</div>
		<div id="ajax_chat_users">			
			<ul>
			</ul>
		</div>
		<div id="ajax_chat_messages">
			<button type="button" id="ajax_chat_btn_back"><i class="fas fa-arrow-left"></i></button>
			<div id="ajax_chat_name"></div>
			<div id="ajax_chat_text"></div>				 
			<form id="ajax_chat_form" action="#">
				<input type="text" id="ajax_chat_input" placeholder="Digita un messaggio..." maxlength="255" autocomplete="off">
			</form>
		</div>
	</div>

	<button type="button" id="ajax_chat_btn_open"><i class="fas fa-comments"></i><span id="ac_online_text">utenti online: <span id="ajax_chat_users_online">0</strong></span><span id="ajax_chat_new_messages"></span></button>

</div>