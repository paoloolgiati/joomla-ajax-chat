<?php
defined('_JEXEC') or die;

//Interfaccia
interface iAjaxChat{
    public function getMessages($data);
    public function sendMessage($data);
	public function setMessageDisplayed($data);	
}

//Funzioni comuni
abstract class ajaxChat{
	
	protected $db;
	protected $query;
	protected $user;
	private $badWords = array(
		'parolaccia1',
		'parolaccia2',
		'parolaccia3',
		'parolaccia4',
		'parolaccia5'
	);
	
	public function __construct(){
		
		$this->db 		= JFactory::getDbo();
		$this->query	= $this->db->getQuery(true);
		$this->user 	= $user = JFactory::getUser();
		
	}
	
	//Lista utenti non bloccati con relativo stato di connessione e numero di nuovi messaggi inviati all'utente loggato
	public function getUsers(){
		
		$sql = "SELECT 
					t1.id, 
					t1.name, 
					(
						SELECT 
							COUNT(userid) 
						FROM 
							#__session 
						WHERE 
							userid = t1.id
					) AS logged, 
					(
						SELECT 
							COUNT(message_id) 
						FROM 
							#__ajax_chat 
						WHERE 
							user_id_from = t1.id 
							AND 
							user_id_to = '" . (int)$this->user->id. "' 
							AND new = '1'
					) AS new_messages
				FROM 
					#__users AS t1				
				WHERE 
					t1.id != '" . (int)$this->user->id. "'
					AND
					t1.block = '0'";
		
		$this->db->setQuery($sql);
		$results = $this->db->loadObjectList();
				
		return $results;
		
	}
	
	//Controllo che l'utente sia valido
	protected function checkUserById($id){
		
		$sql = "SELECT 
					COUNT(id)
				FROM 
					#__users				
				WHERE 
					id = '" . (int)$id. "'
					AND
					block = '0'";
		
		$this->db->setQuery($sql);
		$count = $this->db->loadResult();
		
		return (int)$count;
		
	}
	
	//Nasconde parolacce
	protected function replaceBadWords($message){
		
		foreach($this->badWords as $bw){
			$message = str_replace($bw, str_repeat('*', strlen($bw)), $message);
		}
		
		return $message;
	}
	
}

//Chat su db
class ajaxChatDb extends ajaxChat implements iAjaxChat{
	
	//Lista nuovi messaggi inviati e ricevuti a/da un id utente 
	public function getMessages($data){
		
		$user_id = $data[0];
		$last_message_id = $data[1];
		
		//Controllo che l'utente sia valido
		if($this->checkUserById($user_id) !== 1){
			return 'invalid_user';
		}
		
		$sql = "SELECT 
					* 
				FROM(
				
					SELECT 
						*
					FROM 
						#__ajax_chat				
					WHERE 
						user_id_from = '" . (int)$this->user->id . "'
						AND
						user_id_to = '" . (int)$user_id . "'
					
					UNION

					SELECT 
						*
					FROM 
						#__ajax_chat				
					WHERE 
						user_id_from = '" . (int)$user_id . "'
						AND
						user_id_to = '" . (int)$this->user->id . "'
					
				) AS t1
				
				WHERE 
					t1.message_id > '" . (int)$last_message_id . "'
				ORDER BY
					t1.message_id ASC";
		
		$this->db->setQuery($sql);
		$results = $this->db->loadObjectList();
				
		return $results;
		
	}
	
	//Invia il messaggio ad un altro utente
	public function sendMessage($data){
		
		$user_id = $data[0];
		$message = trim($this->replaceBadWords($data[1]));
		
		//Controllo che l'utente sia valido
		if($this->checkUserById($user_id) !== 1){
			return 'invalid_user';
		}
		
		$sql = "INSERT INTO 
					#__ajax_chat(
						message_id, 
						user_id_from, 
						user_id_to, 
						date_time, 
						message,
						new
					)VALUES(
						NULL, 
						'" . (int)$this->user->id . "', 
						'" . (int)$user_id . "', 
						NOW(), 
						" . $this->db->quote($message) . ",
						'1'
					);";
		
		$this->db->setQuery($sql);				
		$result = $this->db->execute();
		
		return 'ok';
		
	}
	
	//Memorizza che l'utente loggato ha visualizzato un messaggio
	public function setMessageDisplayed($data){
		
		$message_id = $data;
		
		$sql = "UPDATE 
					#__ajax_chat
				SET 
					new = '0'
				WHERE 
					message_id = '" . (int)$message_id . "'
					AND
					user_id_to = '" . (int)$this->user->id . "'";
				
		$this->db->setQuery($sql);				
		$result = $this->db->execute();
		
	}
	
}

//Chat su txt
class ajaxChatTxt extends ajaxChat implements iAjaxChat{
	
	//Lista nuovi messaggi inviati e ricevuti a/da un id utente 
	public function getMessages($data){
				
		return '';
		
	}
	
	//Invia il messaggio ad un altro utente
	public function sendMessage($data){
		
		return '';
		
	}
	
	//Memorizza che l'utente loggato ha visualizzato un messaggio
	public function setMessageDisplayed($data){
		
	}
	
}

//Punto di accesso al modulo tramite com_ajax
class modAjaxChatHelper{
    
	public static function getAjax(){
		
		//Controlla che l'utente sia loggato
		$user = JFactory::getUser();
		if(empty($user->id)){
			return 'auth_error';
		}
		
		//Config modulo
		$module = JModuleHelper::getModule('mod_ajax_chat');
		$params = new JRegistry($module->params);
		$type = $params['type'];
		
		//Memorizza e pulisce le variabili ricevute
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
        $fn  	= $input->get('fn', '', 'string');
		$data  	= $input->get('data', '', 'string');
		
		//Crea istanza dinamicamente
		if($type === 'db'){
			$obj = new ajaxChatDb();	
		}else if($type === 'txt'){
			$obj = new ajaxChatTxt();
		}
		
		//Chiama metodo dinamicamente
		if(method_exists($obj, $fn)){			
			return $obj->{$fn}($data);	
		}else{
			return 'invalid_function';
		}		
		
	}
		
}