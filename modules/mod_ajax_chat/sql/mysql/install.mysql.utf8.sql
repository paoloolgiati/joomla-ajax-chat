CREATE TABLE `#__ajax_chat` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `#__ajax_chat`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `user_id_to` (`user_id_to`),
  ADD KEY `user_id_from` (`user_id_from`);

ALTER TABLE `#__ajax_chat`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;
