<?php
defined('_JEXEC') or die;

//Oggetto utente
$user = JFactory::getUser();

//Mostra il modulo solo se l'utente è loggato
if($user->id != 0){
	
	require JModuleHelper::getLayoutPath('mod_ajax_chat');
	
}